
var numSquares = 6;
var colors = [];
var pickedColor;

var squares = document.querySelectorAll(".square");
var h1 = document.querySelector("h1");
var colorDisplay = document.querySelector("#colorDisplay");
colorDisplay.textContent = pickedColor;
var messageDisplay = document.querySelector("#message");

var resetButton = document.querySelector("#reset");
resetButton.addEventListener("click",function(){
	reset();
});

var modes = document.querySelectorAll(".mode");

init();

function init(){
	
	setUpModes();
	setUpSquares();
	reset();
}


function setUpModes(){
	//mode buttons event listeners
	for (var i = 0; i < modes.length; i++) 
	{
	modes[i].addEventListener("click",function(){
		modes[0].classList.remove("selected");
		modes[1].classList.remove("selected");
		this.classList.add("selected");

		if(this.textContent === "Easy"){
			numSquares = 3;
		}else{
			numSquares =6;
		}
		reset();
	})
	};
}

function setUpSquares(){
	for (var i = 0; i < squares.length; i++) 
	{

	//add click listeners
	squares[i].addEventListener("click",function(){
		//grab color of picked square
		var clickedColor = this.style.background;
		//compare colors
		if(clickedColor === pickedColor){
			messageDisplay.textContent = "Correct";
			changeColors(clickedColor);
			h1.style.background = clickedColor;
			resetButton.textContent = "Play Again?";
		}else{
			this.style.background = "#232323";
			messageDisplay.textContent = "Try Again";
		}

		});
	}
}





function changeColors(color){
	//loop through all squares to match correct color
	for (var i = 0; i < squares.length; i++) {
		squares[i].style.background = color;
	};
}

function pickRandomColor(){
	var randColor = Math.floor(Math.random() * colors.length);
	return colors[randColor];
}

function generateRandomColors(num){
	//make array
	var arr =[];
	//add num to array
	for (var i = 0; i < num; i++) {
		//getRandom color and put into array
		arr.push(randomColor());
	}
	//return array
	return arr;
}

function randomColor(){
	//pick a red,green, blue color from 0 to 255
	var r = Math.floor(Math.random() * 256);
	var g = Math.floor(Math.random() * 256);
	var b = Math.floor(Math.random() * 256);
	return "rgb("+ r + ", " + g + ", " + b + ")";
	
}

function reset(){
	//generate all new colors
	colors = generateRandomColors(numSquares);
	//pick new color
	pickedColor = pickRandomColor();
	//change color display to match new color display
	colorDisplay.textContent = pickedColor;

	messageDisplay.textContent = "";
	//change colors of the squares
	for (var i = 0; i < squares.length; i++) {
		if(colors[i])
		{
			squares[i].style.display = "block";
		squares[i].style.background = colors[i];
		}else{
			squares[i].style.display = "none";
		}
	}

	h1.style.background = "steelblue";
	resetButton.textContent = "New Colors";

}